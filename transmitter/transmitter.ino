#include <SPI.h>  
#include <nRF24L01.h> 
#include <RF24.h> 
RF24           radio(9, 10);  //CE  CSN
int            data[9];    

byte buttonsArray[7] = {2,3,4,5,6,7,8}; //Buttons pin

void setup(){
    Serial.begin(9600);
    for (int i = 0; i < sizeof(buttonsArray); i++){
      pinMode(buttonsArray[i], INPUT_PULLUP);
    }

    radio.begin();              
    radio.setChannel(5);                           
    radio.setDataRate     (RF24_1MBPS);   
    radio.setPALevel      (RF24_PA_HIGH);  
    radio.openWritingPipe (0x1234567890LL);      
}
void loop(){
    data[7] = analogRead(A0);                         
    data[8] = analogRead(A1);                         
    for (int i = 0; i < sizeof(buttonsArray); i++){
      data[i] = !digitalRead(buttonsArray[i]);

    }

    radio.write(&data, sizeof(data));                
}
