#include <SPI.h>                               
#include <nRF24L01.h>  
#include <RF24.h> 
#include <Joystick.h>
#include "Keyboard.h"

RF24           radio(3, 2);                             
int            data[9];
int n = 0;
int buttonsNum = 7;

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_GAMEPAD,
                   7, 0, // Button Count, Hat Switch Count
                   true, true, false, // X Y, no Z
                   false, false, false, // Rx, Ry, Rz
                   false, false, // No rudder or throttle
                   false, false, false); // No accelerator, brake, or steering


void setup(){
    Serial.begin(9600);
    delay(1000);
    radio.begin(); 
    radio.setChannel(5); 
    radio.setDataRate     (RF24_1MBPS);
    radio.setPALevel      (RF24_PA_HIGH);
    radio.openReadingPipe (1, 0x1234567890LL);
    radio.startListening  ();    
    
    Joystick.begin(true);
    Joystick.setXAxisRange(5, 700);
    Joystick.setYAxisRange(5, 700);
    Joystick.setXAxis(700);
    
    Serial.println("Start");

//  radio.stopListening   (); 
}
void loop(){
    if(radio.available()){              
        radio.read(&data, sizeof(data));               
        Joystick.setXAxis(data[7]);
        Joystick.setYAxis(data[8]);
        for (int i = 0; i < (sizeof(data) / 2); i++){
          Serial.print(data[i]);
          Serial.print("  ");
        }
        for (int i = 0; i < buttonsNum; i++){
          Joystick.setButton(i, data[i]);
        }
          
        Serial.println("");

    }
}
